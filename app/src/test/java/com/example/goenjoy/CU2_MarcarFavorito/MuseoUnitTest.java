package com.example.goenjoy.CU2_MarcarFavorito;

import com.example.goenjoy.model.Museo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.lang.reflect.Field;

public class MuseoUnitTest {

    @Test
    public void constructorTest(){
        com.example.goenjoy.model.Museo instance = new  com.example.goenjoy.model.Museo(0, "title", "relation", "localidad", "postalCode", "streetAdress",
                40.414358466555235f, -3.6974741545860015f, "desc", "0", "schedule", 1, 1, 1,1);
        assertEquals(instance.getId(),0);
        assertEquals(instance.getTitle(),"title");
        assertEquals(instance.getRelation(),"relation");
        assertEquals(instance.getLocalidad(),"localidad");
        assertEquals(instance.getPostalCode(),"postalCode");
        assertEquals(instance.getStreetAdress(),"streetAdress");
        assertTrue("",instance.getLatitude()==40.414358466555235f);
        assertTrue("",instance.getLongitude()==-3.6974741545860015f);
        assertEquals(instance.getDesc(),"desc");
        assertEquals(instance.getAccesibility(),"0");
        assertEquals(instance.getSchedule(),"schedule");
        assertEquals(instance.getFav(),1);
        assertEquals(instance.getDeseo(),1);
        assertEquals(instance.getRuta(),1);
        assertEquals(instance.getTipo(),1);
    }

    @Test
    public void setIdTest() throws NoSuchFieldException, IllegalAccessException {
        int value = 506;
        com.example.goenjoy.model.Museo instance = new  com.example.goenjoy.model.Museo();
        instance.setId(value);
        final Field field = instance.getClass().getDeclaredField("id");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getIdTest() throws NoSuchFieldException, IllegalAccessException {
        final  com.example.goenjoy.model.Museo instance = new  com.example.goenjoy.model.Museo();
        final Field field = instance.getClass().getDeclaredField("id");
        field.setAccessible(true);
        field.set(instance, 12);

        //when
        final int result = instance.getId();

        //then
        assertEquals("field wasn't retrieved properly", result, 12);
    }

    @Test
    public void setFav() throws NoSuchFieldException, IllegalAccessException {
        int value = 1;
        com.example.goenjoy.model.Museo instance = new  com.example.goenjoy.model.Museo();
        instance.setFav(value);
        final Field field = instance.getClass().getDeclaredField("fav");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getFav() throws NoSuchFieldException, IllegalAccessException {
        final  com.example.goenjoy.model.Museo instance = new  com.example.goenjoy.model.Museo();
        final Field field = instance.getClass().getDeclaredField("fav");
        field.setAccessible(true);
        field.set(instance, 1);

        //when
        final int result = instance.getFav();

        //then
        assertEquals(result,1);
    }
}