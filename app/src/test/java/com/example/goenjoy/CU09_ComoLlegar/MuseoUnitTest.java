package com.example.goenjoy.CU09_ComoLlegar;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Field;

public class MuseoUnitTest {

    @Test
    public void setLatitudTest() throws NoSuchFieldException, IllegalAccessException {
        float value = -43.9534578345f;
        com.example.goenjoy.model.Museo instance = new  com.example.goenjoy.model.Museo();
        instance.setLatitude(value);
        final Field field = instance.getClass().getDeclaredField("latitude");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getLatitudTest() throws NoSuchFieldException, IllegalAccessException {
        final  com.example.goenjoy.model.Museo instance = new  com.example.goenjoy.model.Museo();
        final Field field = instance.getClass().getDeclaredField("latitude");
        field.setAccessible(true);
        field.set(instance, -43.9534578345f);

        final float result = instance.getLatitude();

        assertTrue(result == -43.9534578345f);
    }

    @Test
    public void setLogitudTest() throws NoSuchFieldException, IllegalAccessException {
        float value = -43.9534578345f;
        com.example.goenjoy.model.Museo instance = new  com.example.goenjoy.model.Museo();
        instance.setLongitude(value);
        final Field field = instance.getClass().getDeclaredField("longitude");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getLongitudTest() throws NoSuchFieldException, IllegalAccessException {
        final  com.example.goenjoy.model.Museo instance = new  com.example.goenjoy.model.Museo();
        final Field field = instance.getClass().getDeclaredField("longitude");
        field.setAccessible(true);
        field.set(instance, -43.9534578345f);

        final float result = instance.getLongitude();

            assertTrue(result == -43.9534578345f);
    }

}
