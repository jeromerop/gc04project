package com.example.goenjoy.CU13_AnadirDeseo;

import com.example.goenjoy.model.Museo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

import java.lang.reflect.Field;

public class MuseoUnitTest {

    @Test
    public void setDeseoTest() throws NoSuchFieldException, IllegalAccessException {
        int value = 1;
        com.example.goenjoy.model.Museo instance = new  com.example.goenjoy.model.Museo();
        instance.setDeseo(value);
        final Field field = instance.getClass().getDeclaredField("deseo");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getDeseoTest() throws NoSuchFieldException, IllegalAccessException {
        final  com.example.goenjoy.model.Museo instance = new  com.example.goenjoy.model.Museo();
        final Field field = instance.getClass().getDeclaredField("deseo");
        field.setAccessible(true);
        field.set(instance, 1);

        //when
        final int result = instance.getDeseo();

        //then
        assertEquals(result,1);
    }

}