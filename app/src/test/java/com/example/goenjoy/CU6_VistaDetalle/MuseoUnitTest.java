package com.example.goenjoy.CU6_VistaDetalle;

import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MuseoUnitTest {
    @Test
    public void setTitleTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "Museo del Prado";
        com.example.goenjoy.model.Museo instance = new  com.example.goenjoy.model.Museo();
        instance.setTitle(value);
        final Field field = instance.getClass().getDeclaredField("title");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getTitleTest() throws NoSuchFieldException, IllegalAccessException {
        final  com.example.goenjoy.model.Museo instance = new  com.example.goenjoy.model.Museo();
        final Field field = instance.getClass().getDeclaredField("title");
        field.setAccessible(true);
        field.set(instance, "Museo del Prado");

        //when
        final String result = instance.getTitle();

        //then
        assertEquals("field wasn't retrieved properly", result, "Museo del Prado");
    }

    @Test
    public void setLocalidadTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "Cáceres";
        com.example.goenjoy.model.Museo instance = new  com.example.goenjoy.model.Museo();
        instance.setLocalidad(value);
        final Field field = instance.getClass().getDeclaredField("localidad");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getLocalidadTest() throws NoSuchFieldException, IllegalAccessException {
        final  com.example.goenjoy.model.Museo instance = new  com.example.goenjoy.model.Museo();
        final Field field = instance.getClass().getDeclaredField("localidad");
        field.setAccessible(true);
        field.set(instance, "Cáceres");

        //when
        final String result = instance.getLocalidad();

        //then
        assertEquals("field wasn't retrieved properly", result, "Cáceres");
    }

    @Test
    public void setPostalCodeTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "10001";
        com.example.goenjoy.model.Museo instance = new  com.example.goenjoy.model.Museo();
        instance.setPostalCode(value);
        final Field field = instance.getClass().getDeclaredField("postalCode");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getPostalCodeTest() throws NoSuchFieldException, IllegalAccessException {
        final  com.example.goenjoy.model.Museo instance = new  com.example.goenjoy.model.Museo();
        final Field field = instance.getClass().getDeclaredField("postalCode");
        field.setAccessible(true);
        field.set(instance, "10001");

        //when
        final String result = instance.getPostalCode();

        //then
        assertEquals("field wasn't retrieved properly", result, "10001");
    }

    @Test
    public void setStreetAdressTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "Avenida de la Universidad";
        com.example.goenjoy.model.Museo instance = new  com.example.goenjoy.model.Museo();
        instance.setStreetAdress(value);
        final Field field = instance.getClass().getDeclaredField("streetAdress");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getStreetAdressTest() throws NoSuchFieldException, IllegalAccessException {
        final  com.example.goenjoy.model.Museo instance = new  com.example.goenjoy.model.Museo();
        final Field field = instance.getClass().getDeclaredField("streetAdress");
        field.setAccessible(true);
        field.set(instance, "Avenida de la Universidad");

        //when
        final String result = instance.getStreetAdress();

        //then
        assertEquals("field wasn't retrieved properly", result, "Avenida de la Universidad");
    }

    @Test
    public void setDescTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "Un museo muy bonito";
        com.example.goenjoy.model.Museo instance = new  com.example.goenjoy.model.Museo();
        instance.setDesc(value);
        final Field field = instance.getClass().getDeclaredField("desc");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getDescTest() throws NoSuchFieldException, IllegalAccessException {
        final  com.example.goenjoy.model.Museo instance = new  com.example.goenjoy.model.Museo();
        final Field field = instance.getClass().getDeclaredField("desc");
        field.setAccessible(true);
        field.set(instance, "Un museo muy bonito");

        //when
        final String result = instance.getDesc();

        //then
        assertEquals("field wasn't retrieved properly", result, "Un museo muy bonito");
    }

    @Test
    public void setAccesibilityTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "1,6";
        com.example.goenjoy.model.Museo instance = new  com.example.goenjoy.model.Museo();
        instance.setAccesibility(value);
        final Field field = instance.getClass().getDeclaredField("accesibility");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getAccesibilityTest() throws NoSuchFieldException, IllegalAccessException {
        final  com.example.goenjoy.model.Museo instance = new  com.example.goenjoy.model.Museo();
        final Field field = instance.getClass().getDeclaredField("accesibility");
        field.setAccessible(true);
        field.set(instance, "1,6");

        //when
        final String result = instance.getAccesibility();

        //then
        assertEquals("field wasn't retrieved properly", result, "1,6");
    }

}
