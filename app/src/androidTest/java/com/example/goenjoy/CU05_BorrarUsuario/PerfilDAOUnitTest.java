package com.example.goenjoy.CU05_BorrarUsuario;

import android.app.Application;
import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.room.Room;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.test.platform.app.InstrumentationRegistry;

import com.example.goenjoy.model.Perfil;
import com.example.goenjoy.room.MuseoDatabase;
import com.example.goenjoy.room.PerfilDao;
import com.example.goenjoy.room.Repository;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;


@RunWith(AndroidJUnit4.class)
public class PerfilDAOUnitTest {

    private PerfilDao perfilDAO;
    private MuseoDatabase mDatabase;


    @Rule
    public TestRule rule = new InstantTaskExecutorRule();


    @Before
    public void createMuseoDatabase() {

        Context context = InstrumentationRegistry.getInstrumentation().getTargetContext();
        mDatabase = Room.inMemoryDatabaseBuilder(context, MuseoDatabase.class).allowMainThreadQueries().build();
        perfilDAO = mDatabase.perfilDao();
    }

    @After
    public void closeDb() throws IOException {
        //closing database
        mDatabase.close();
    }

    @Test
    public void deletePerfilAndReadInDataBase() throws Exception {

        Perfil perfil = new Perfil(1, "Federico", "federicopizarro@gmail.com");

        perfilDAO.insert(perfil);

        LiveData<List<Perfil>> liveperfiles = perfilDAO.get();
        List<Perfil> perfiles = LiveDataTestUtils.getValue(liveperfiles);

        perfilDAO.delete();

        liveperfiles = perfilDAO.get();
        perfiles = LiveDataTestUtils.getValue(liveperfiles);

        assertTrue(perfiles.isEmpty());

    }

}