package com.example.goenjoy.room;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;

import com.example.goenjoy.api.ApiInterfaz;
import com.example.goenjoy.model.Museo;
import com.example.goenjoy.model.Perfil;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Repository {
    private MuseoDao mMuseoDao;
    private PerfilDao mPerfilDao;
    private LiveData<List<Museo>> mAllLugares;
    private LiveData<List<Museo>> mAllMuseos;
    private LiveData<List<Museo>> mAllParques;
    private LiveData<List<Museo>> mAllMuseosDes;
    private LiveData<List<Museo>> mAllMuseosRuta;
    private LiveData<List<Perfil>> mPerfil;
    private LiveData<List<Museo>> mAllMuseosFav;

    public Repository(Application application) {
        MuseoDatabase db = MuseoDatabase.getDatabase(application);
        mMuseoDao = db.museoDao();
        mAllLugares = mMuseoDao.getAll();
        mAllMuseos = mMuseoDao.getAllMuseo();
        mAllParques = mMuseoDao.getAllParque();
        mAllMuseosDes = mMuseoDao.getAllDes();
        mAllMuseosRuta = mMuseoDao.getAllRuta();
        mAllMuseosFav = mMuseoDao.getAllFav();
        mPerfilDao = db.perfilDao();
        mPerfil = mPerfilDao.get();
    }

    public LiveData<List<Museo>> getAllLugares() {
        return mAllLugares;
    }
    public LiveData<List<Museo>> getAllMuseos() {return mAllMuseos;}
    public LiveData<List<Museo>> getAllParques(){return mAllParques;}


    public LiveData<List<Museo>> getAllDes() {
        return mAllMuseosDes;
    }

    public LiveData<List<Museo>> getAllRuta() {
        return mAllMuseosRuta;
    }
    public LiveData<List<Museo>> getAllFav() {
        return mAllMuseosFav;
    }
    public LiveData<List<Perfil>> getAllProfile() {
        return mPerfil;
    }

    public boolean existe(String title) {
        final boolean[] existe = {false};
        MuseoDatabase.databaseWriteExecutor.execute(new Runnable() {
            //hilo en SEGUNDO PLANO
            @Override
            public void run() {
                Museo x = mMuseoDao.getMuseo(title);
                if (x != null) {
                    if (x.getTitle().equals(title)) {
                        existe[0] = true;
                    }
                }

            }
        });
        //HACEMOS EL SLEEP PARA QUE EL HILO EN SEGUNDO PLANO NOS
        // DEVUELVA LA RESPUESTA ANTES DE QUE SIGA EL HILO PRINCIPAL
        try {
            Thread.sleep(55);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return existe[0];

    }

    public void insert(final Museo museo) {
        MuseoDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                mMuseoDao.insert(museo);
            }
        });
    }
    public void insertPerfil(final Perfil perfil) {
        MuseoDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                mPerfilDao.insert(perfil);
            }
        });
    }

    public void update(final Perfil perfil) {
        MuseoDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                mPerfilDao.update(perfil);
            }
        });
    }


    public void update(final Museo museo) {
        MuseoDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                mMuseoDao.update(museo);
            }
        });
    }

    public void delete(final Museo museo) {
        MuseoDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                mMuseoDao.delete(museo);
            }
        });
    }

    public void deleteAll() {
        MuseoDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                mMuseoDao.deleteAll();
            }
        });
    }

    public void deleteProfile() {
        MuseoDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                mPerfilDao.delete();
            }
        });
    }


    public void cargarDatos(){
        ApiInterfaz service = ApiInterfaz.retrofit.create(ApiInterfaz.class);
        //-- Cargamos los museos --
        Call<JsonObject> callMuseo = service.getMuseos();
        callMuseo.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonArray museoJsonArray = response.body().getAsJsonArray("@graph");
                for(int i= 0 ; i < museoJsonArray.size(); i++){
                    Museo museo = new Museo();
                    JsonObject item = museoJsonArray.get(i).getAsJsonObject();
                    if(item.get("id") != null){museo.setId(item.get("id").getAsInt());}
                    if(item.get("title") != null){museo.setTitle(item.get("title").toString().substring(1, item.get("title").toString().length()-1));}
                    if(item.get("relation") != null){ museo.setRelation(item.get("relation").toString().substring(1, item.get("relation").toString().length()-1));}
                    if(item.get("address").getAsJsonObject().get("locality") != null){museo.setLocalidad(item.get("address").getAsJsonObject().get("locality").toString().substring(1, item.get("address").getAsJsonObject().get("locality").toString().length()-1));}
                    if(item.get("address").getAsJsonObject().get("postal-code") != null){museo.setPostalCode(item.get("address").getAsJsonObject().get("postal-code").toString().substring(1, item.get("address").getAsJsonObject().get("postal-code").toString().length()-1));}
                    if(item.get("address").getAsJsonObject().get("street-address") != null){museo.setStreetAdress(item.get("address").getAsJsonObject().get("street-address").toString().substring(1, item.get("address").getAsJsonObject().get("street-address").toString().length()-1));}
                    if(item.get("location") != null){museo.setLatitude(item.get("location").getAsJsonObject().get("latitude").getAsFloat());}
                    if(item.get("location") != null){museo.setLongitude(item.get("location").getAsJsonObject().get("longitude").getAsFloat());}
                    if(item.get("organization").getAsJsonObject().get("organization-desc") != null){museo.setDesc(item.get("organization").getAsJsonObject().get("organization-desc").toString().substring(1, item.get("organization").getAsJsonObject().get("organization-desc").toString().length()-1));}
                    if(item.get("organization").getAsJsonObject().get("accesibility") != null){museo.setAccesibility(item.get("organization").getAsJsonObject().get("accesibility").toString().substring(1, item.get("organization").getAsJsonObject().get("accesibility").toString().length()-1));}
                    if(item.get("organization").getAsJsonObject().get("schedule") != null){museo.setSchedule(item.get("organization").getAsJsonObject().get("schedule").toString().substring(1, item.get("organization").getAsJsonObject().get("schedule").toString().length()-1));}
                    museo.setFav(0);
                    museo.setRuta(0);
                    museo.setDeseo(0);
                    museo.setTipo(1); //Al ser museo es 1
                    if(!existe(museo.getTitle())){
                        insert(museo);
                    }

                }
            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e("ErrorCargaMuseos", t.getMessage());
            }
        });

        //-- Cargamos los parques --
        Call<JsonObject> callParques = service.getParques();
        callParques.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonArray museoJsonArray = response.body().getAsJsonArray("@graph");
                for(int i= 0 ; i < museoJsonArray.size(); i++){
                    Museo museo = new Museo();
                    JsonObject item = museoJsonArray.get(i).getAsJsonObject();
                    if(item.get("id") != null){museo.setId(item.get("id").getAsInt());}
                    if(item.get("title") != null){museo.setTitle(item.get("title").toString().substring(1, item.get("title").toString().length()-1));}
                    if(item.get("relation") != null){ museo.setRelation(item.get("relation").toString().substring(1, item.get("relation").toString().length()-1));}
                    if(item.get("address").getAsJsonObject().get("locality") != null){museo.setLocalidad(item.get("address").getAsJsonObject().get("locality").toString().substring(1, item.get("address").getAsJsonObject().get("locality").toString().length()-1));}
                    if(item.get("address").getAsJsonObject().get("postal-code") != null){museo.setPostalCode(item.get("address").getAsJsonObject().get("postal-code").toString().substring(1, item.get("address").getAsJsonObject().get("postal-code").toString().length()-1));}
                    if(item.get("address").getAsJsonObject().get("street-address") != null){museo.setStreetAdress(item.get("address").getAsJsonObject().get("street-address").toString().substring(1, item.get("address").getAsJsonObject().get("street-address").toString().length()-1));}
                    if(item.get("location") != null){museo.setLatitude(item.get("location").getAsJsonObject().get("latitude").getAsFloat());}
                    if(item.get("location") != null){museo.setLongitude(item.get("location").getAsJsonObject().get("longitude").getAsFloat());}
                    if(item.get("organization").getAsJsonObject().get("organization-desc") != null){museo.setDesc(item.get("organization").getAsJsonObject().get("organization-desc").toString().substring(1, item.get("organization").getAsJsonObject().get("organization-desc").toString().length()-1));}
                    if(item.get("organization").getAsJsonObject().get("accesibility") != null){museo.setAccesibility(item.get("organization").getAsJsonObject().get("accesibility").toString().substring(1, item.get("organization").getAsJsonObject().get("accesibility").toString().length()-1));}
                    if(item.get("organization").getAsJsonObject().get("schedule") != null){museo.setSchedule(item.get("organization").getAsJsonObject().get("schedule").toString().substring(1, item.get("organization").getAsJsonObject().get("schedule").toString().length()-1));}
                    museo.setFav(0);
                    museo.setRuta(0);
                    museo.setDeseo(0);
                    museo.setTipo(2); //Al ser parque es 2
                    if(!existe(museo.getTitle())){
                        insert(museo);
                    }

                }
            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e("ErrorCargaMuseos", t.getMessage());
            }
        });
    }
}