package com.example.goenjoy.room;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.goenjoy.model.Actualizacion;

import java.util.List;

@Dao
public interface ActualizarDao {
    @Query("SELECT * FROM actualizacion")
    public List<Actualizacion> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insert(Actualizacion act);

    @Update
    public void update(Actualizacion act);

    @Query("DELETE FROM actualizacion")
    public void deleteAll();
}