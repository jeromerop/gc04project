package com.example.goenjoy.viewmodel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import com.example.goenjoy.model.Perfil;
import com.example.goenjoy.room.Repository;
import java.util.List;

public class PerfilViewModel extends AndroidViewModel{

    private Repository mRepository;
    private LiveData<List<Perfil>> mPerfil;


    public PerfilViewModel(Application application) {
        super(application);
        mRepository = new Repository(application);
        mPerfil = mRepository.getAllProfile();
    }

    public LiveData<List<Perfil>> get() { return mPerfil; }
    public void insert(Perfil perfil) { mRepository.insertPerfil(perfil); }
    public void delete() { mRepository.deleteProfile(); }
    public void update(Perfil perfil) { mRepository.update(perfil);}
}
