//*
package com.example.goenjoy.viewmodel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import com.example.goenjoy.model.Museo;
import com.example.goenjoy.room.Repository;

public class DetallesViewModel extends AndroidViewModel {
    private Repository mRepository;

    public DetallesViewModel(Application application) {
        super(application);
        mRepository = new Repository(application);
    }

    public void insert(Museo museo) { mRepository.insert(museo); }
    public void update(Museo museo) { mRepository.update(museo); }
    public void delete(Museo museo) { mRepository.delete(museo); }
}